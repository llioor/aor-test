// in src/Dashboard.js
import React from 'react';
import { Card, CardText } from 'material-ui/Card';
import { ViewTitle } from 'admin-on-rest/lib/mui';
import Initialization from './../../components/Initialization';

export default () => (
  <Card>
    <ViewTitle title="Dashboard" />
    <CardText>Lorem ipsum sic dolor amet...</CardText>
    <Initialization/>
  </Card>
);