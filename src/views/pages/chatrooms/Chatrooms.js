import React from 'react';
import {
  List, Edit, Create, Show, Datagrid, SimpleShowLayout, SimpleForm, Filter,
  TextField, DateField, EditButton, DeleteButton,
  TextInput, DisabledInput, LongTextInput, BooleanInput, RadioButtonGroupInput,
  ReferenceInput, SelectInput
} from 'admin-on-rest';

// import { SelectField } from 'admin-on-rest/lib/mui/field';

// import ApproveButton from './../../components/ApproveButton'

const ChatroomFilter = (props) => (
  <Filter {...props}>
    <ReferenceInput label="Country" source="country_id" reference="countries" allowEmpty translateChoice={false}>
      <SelectInput optionText="title" />
    </ReferenceInput>
    <TextInput label="Type" source="type" off />
  </Filter>
);

export const ChatroomsList = (props) => (
  <List {...props} title="Chatrooms List"  filters={<ChatroomFilter />} >
    <Datagrid>
      {/*<TranslateField source="translations" />*/}
      <TextField source="title" />
      <TextField source="type" />
      <TextField source="country.title" label="country" />
      <TextField source="total_users" />
      <EditButton record='' />
      <DeleteButton />
    </Datagrid>
  </List>
);

const ChatroomTitle = ({ record }) => {
  return <span>Chatroom {record ? `"${record.title}"` : ''}</span>;
};

export const ChatroomEdit = (props) => (
  <Edit title={<ChatroomTitle />} {...props}>
    <SimpleForm>
      <RadioButtonGroupInput  source="type" choices={[
        { id: 'PUBLIC', name: 'Public' },
        { id: 'PRIVATE', name: 'Private' },
      ]} />
      <DisabledInput source="uuid"/>
      <TextInput source="title" />
      <TextInput source="short_description" />
      <LongTextInput source="description" />

      <TextInput source="address" />
      <TextInput source="city" />

      {/*<ReferenceInput label="Country" source="country_id" reference="countries" allowEmpty translateChoice={false}>*/}
        {/*<SelectInput optionText="title" source="uuid" />*/}
      {/*</ReferenceInput>*/}

      <LongTextInput source="polygon.coordinates" label="Polygon points in format of '[latitude] [longitude] (start point), [latitude] [longitude], [latitude] [longitude] (start point again)' "/>

      <BooleanInput label="Is active?" source="is_active" defaultValue={false} />
    </SimpleForm>
  </Edit>
);

export const ChatroomCreate = (props) => (
  <Create {...props}>
    <SimpleForm>

      <RadioButtonGroupInput  source="type" defaultValue={'PUBLIC'} choices={[
        { id: 'PUBLIC', name: 'Public' },
        { id: 'PRIVATE', name: 'Private' },
      ]} />

      <TextInput source="title"/>
      <TextInput source="short_description" />
      <LongTextInput source="description" />

      <TextInput source="address" />
      <TextInput source="city" />
      <TextInput source="country_id" />
      {/*<ReferenceInput label="Country" source="country_id" reference="countries" translateChoice={false}>*/}
        {/*<SelectInput optionText="title" source="uuid" />*/}
      {/*</ReferenceInput>*/}

      <LongTextInput source="polygon" label="Polygon points in format of '[latitude] [longitude] (start point), [latitude] [longitude], [latitude] [longitude] (start point again)' "/>

      <BooleanInput label="Is active?" source="is_active" defaultValue={false} />

    </SimpleForm>
  </Create>
);

export const ChatroomShow = ({ ...props }) => (
    <Show {...props}>
      <SimpleShowLayout>
        <TextField source="id" />
        <TextField source="address" />
        <DateField source="created_at" />
        <TextField source="city" />
      </SimpleShowLayout>
    </Show>
);
