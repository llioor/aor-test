import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { localesActions } from '../../redux/locales';
import { profileActions } from '../../redux/profile';

class Initialization extends Component {

  componentWillMount() {
    this.props.getLocales();
    this.props.getProfile();
  }

  render() {
    const { locales, profile } = this.props;
    console.log('App locales:', locales);
    console.log('User profile:', profile);
    const lanaugesList = locales.map(function (language, i) {
          return <li key={i}>{language.native}</li>;
        }
    );
    return <ul>{lanaugesList}</ul>;
  }
}

Initialization.propTypes = {
  getLocales: PropTypes.func,
  locales: PropTypes.array,
  getProfile: PropTypes.func,
  profile: PropTypes.array,
};

const mapStateToProps = state => ({
  locales: state.locales,
  profile: state.profile,
});

const mapDispatchToProps = {
  getLocales: localesActions.getLocales,
  getProfile: profileActions.getProfile,
};

export default connect(mapStateToProps, mapDispatchToProps)(Initialization);
