import types from './types';
import actions from './actions';
import { call, put, takeEvery } from 'redux-saga/effects';
import fetchClient from './../../utils/fetchClient';

function* getProfileSaga() {
  try {
    yield put(actions.getProfileLoading());
    let me;
    me = yield call(fetchProfile);
    yield put(actions.getProfileReceived(me));
  } catch (error) {
    yield put(actions.getProfileFailed(error));
  }
}

function fetchProfile() {
  console.log('Get');
  return fetchClient.get('admin/profile/me')
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
}

export default function* profielSaga() {
  yield [
    takeEvery(types.GET_PROFILE, getProfileSaga),
  ];
};
