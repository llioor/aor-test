import types from './types';

const getProfile = () => ({
  type: types.GET_PROFILE,
});

const getProfileLoading = () => ({
  type: types.GET_PROFILE_LOADING,
});

const getProfileReceived = (profile) => ({
  type: types.GET_PROFILE_RECEIVED,
  payload: profile,
});

const getProfileFailed = (error) => ({
  type: types.GET_PROFILE_FAILURE,
});

export default {
  getProfile,
  getProfileLoading,
  getProfileReceived,
  getProfileFailed,
};
