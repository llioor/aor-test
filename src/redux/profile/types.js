const GET_PROFILE = 'app/profile/GET_PROFILE';
const GET_PROFILE_RECEIVED = 'app/profile/GET_PROFILE_RECEIVED';
const GET_PROFILE_LOADING = 'app/profile/GET_PROFILE_LOADING';
const GET_PROFILE_FAILURE = 'app/profile/GET_PROFILE_FAILURE';

export default {
  GET_PROFILE,
  GET_PROFILE_RECEIVED,
  GET_PROFILE_LOADING,
  GET_PROFILE_FAILURE,
};
