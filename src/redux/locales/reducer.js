import types from "./types";

/* State Shape
 {
 locales: array
 }
 */

export default (state = [], action) => {
  switch( action.type ) {
    case types.GET_LOCALES: {
      console.log('in reducer');
        // change parameter to prevent double languages select
      return state;
    }

    case types.GET_LOCALES_LOADING: {
        // show loader on languages dropdown
      return state;
    }
    case types.GET_LOCALES_RECEIVED: {
      // change parameter to enable languages select
      // set the locales in state
      return action.payload;
    }

    default: return state;
  }
}