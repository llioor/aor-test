import types from './types';
import actions from './actions';
import { call, put, takeEvery } from 'redux-saga/effects';
import fetchClient from './../../utils/fetchClient';

function* getLocalesSaga() {
  try {
    yield put(actions.loadingLocales());
    let locales;
    locales = yield call(fetchLocales);
    yield put(actions.localesReceived(locales.languages));
  } catch (error) {
    yield put(actions.failedLocales(error));
  }
}


function fetchLocales() {
  return fetchClient.get('locales')
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
}

export default function* localesSaga() {
  yield [
    takeEvery(types.GET_LOCALES, getLocalesSaga),
  ];
};
