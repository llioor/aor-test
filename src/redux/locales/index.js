import reducer from './reducer';

export { default as localesActions } from './actions';
export { default as localesSaga } from './operations';
export { default as localesTypes } from './types';

export default reducer;