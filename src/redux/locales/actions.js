import types from './types';

const getLocales = () => ({
  type: types.GET_LOCALES,
});

const loadingLocales = () => ({
  type: types.GET_LOCALES_LOADING,
});

const localesReceived = (locales) => ({
  type: types.GET_LOCALES_RECEIVED,
  payload: locales,
});

const failedLocales = (error) => ({
  type: types.GET_LOCALES_FAILURE,
});



export default {
  getLocales,
  loadingLocales,
  localesReceived,
  failedLocales,
};