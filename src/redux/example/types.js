const QUACK = "app/duck/QUACK";
const SWIM = "app/duck/SWIM";

export default {
  QUACK,
  SWIM
};