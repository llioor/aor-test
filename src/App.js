import React from 'react';
import { Admin, Resource, Delete, englishMessages } from 'admin-on-rest';

// Your app utils
import restClient from './utils/restClient';
import authClient from './utils/authClient';
import localReducer, { localesSaga } from './redux/locales';
import profileReducer, { profileSaga } from './redux/profile';

// Your app pages & components
import Dashboard from './views/pages/dashboard/Dashboard';
import { ChatroomsList, ChatroomCreate, ChatroomEdit, ChatroomShow } from './views/pages/chatrooms/Chatrooms';

// Custom Reducers
const customReducers = {
  locales: localReducer,
  profile: profileReducer,
};

// Custom Sagas
const customSagas = [
  localesSaga,
  profileSaga,
];

// Your app labels
const adminMessages = {
  en: {},
};
const messages = {
  en: { ...englishMessages, ...adminMessages },
};

// bootstrap redux and the routes
const App = () => (
    <Admin
        title="System Admin"
        dashboard={ Dashboard }
        restClient={ restClient }
        authClient={ authClient }
        customReducers={ customReducers }
        customSagas={ customSagas }
        messages={ messages }>
          <Resource name="chatrooms" list={ChatroomsList} create={ChatroomCreate} edit={ChatroomEdit} show={ChatroomShow} remove={Delete} />
    </Admin>
);

export default App;
