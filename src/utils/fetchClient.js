import axios from 'axios';

const fetchClient = () => {
  // For initialization we take the token from the localStorage
  const token = localStorage.getItem('token');
  console.log('token:', token);

  const defaultOptions = {
    baseURL: process.env.REACT_APP_API_PATH,
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
      Authorization: token ? `Bearer ${token}` : '',
    },
  };

  // defaults.headers.common.Authorization
  return axios.create(defaultOptions);
};

export default fetchClient();
